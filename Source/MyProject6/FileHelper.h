// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "Core.h"
#include "CoreUObject.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FileHelper.generated.h"

#define UUID TEXT("uuid")
#define SERVER_URL TEXT("server-url")
#define CONFIG_DIR TEXT("./Config")
#define CONFIG_FILE TEXT("client.cfg")
#define REGISTERED_CLIENT TEXT("registered-client")
#define PHRASE TEXT("phrase")
#define NUM_COMPLETED TEXT("number-of-completed")

DECLARE_LOG_CATEGORY_EXTERN(FileHelperLog, Log, Log);

USTRUCT()
struct FConfigs
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "FileHelper|Configs")
	FString Uuid;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "FileHelper|Configs")
	FString ServerUrl;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "FileHelper|Configs")
	FString RegisteredClient;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "FileHelper|Configs")
	FString Phrase;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "FileHelper|Configs")
	FString NumCompleted;

	FConfigs()
	{
		Uuid = TEXT("");
		ServerUrl = TEXT("https://bubble.eu-west-1.elasticbeanstalk.com");
		RegisteredClient = TEXT("");
		Phrase = TEXT("");
		NumCompleted = TEXT("0");
	}

};

/**
 * 
 */
UCLASS(BlueprintType, BLueprintable)
class UFileHelper : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "FileHelper")
	static bool WriteStringToFile(FString JoyfulFileName, FString SaveText, bool AllowOverWriting);

	UFUNCTION(BlueprintCallable, Category = "FleHelper")
	static FString ReadStringFromFile(FString JoyfulFileName);

	UFUNCTION(BlueprintCallable, Category = "FleHelper|Config")
	static FConfigs ReadConfigs();

	UFUNCTION(BlueprintCallable, Category = "FleHelper|Config")
	static bool WriteConfigs(FConfigs Configs);

private:
	static FString GetGameFilePath(FString JoyfulFileName);
	
};
