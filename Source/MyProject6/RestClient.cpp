// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject6.h"
#include "RestClient.h"

DEFINE_LOG_CATEGORY(RestLog);

URestClient::URestClient(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	FirstPage = "1";
	LastPage = "last";
	OrderScoreDesc = "score";
	OrderAltitudeDesc = "altitude";
}

URestClient* URestClient::GetRestClient() {
	return NewObject<URestClient>(URestClient::StaticClass());
}

void URestClient::GetUUIDFromServer(const FString ServerUrl, const FString Phrase, const FString Name)
{
	TSharedRef<IHttpRequest> HttpRequest = FHttpModule::Get().CreateRequest();

	FString Url = ServerUrl + TEXT("/registered_client/");
	HttpRequest->SetURL(Url);
	HttpRequest->SetVerb(TEXT("POST"));
	HttpRequest->SetHeader(TEXT("Content-Type"), TEXT("application/json"));

	FString json = CreateRegisteredClientJson(Phrase, Name);

	HttpRequest->SetContentAsString(json);
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &URestClient::OnProcessRequestComplete);

	HttpRequest->ProcessRequest();
}

FString URestClient::CreateRegisteredClientJson(const FString Phrase, const FString Name)
{
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
	JsonObject->SetStringField("phrase", Phrase);
	JsonObject->SetStringField("user_name", Name);

	FString OutputString;
	TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&OutputString);
	FJsonSerializer::Serialize(JsonObject.ToSharedRef(), Writer);

	return OutputString;
}

FString URestClient::GetUUIDFromServerResponse()
{
	FString UUIDString = "";

	if (bIsValidJsonResponse)
	{
		UUIDString = JsonParsed->GetStringField("uuid");
	}

	return UUIDString;
}

FString URestClient::GetUserNameFromServerResponse()
{
	FString UserName = "";

	if (bIsValidJsonResponse)
	{
		UserName = JsonParsed->GetStringField("user_name");
	}

	return UserName;
}

FString URestClient::GetErrorFromServerResponse()
{
	FString Error = "";

	if (ResponseCode != 200 && ResponseCode != 201)
	{
		if (JsonParsed->HasField("user_name")) 
		{
			TArray<TSharedPtr<FJsonValue>> DataArrayObject = JsonParsed->GetArrayField("user_name");
			TSharedPtr<FJsonValue> UUID = DataArrayObject[0];
			Error = UUID->AsString();
		}
	}

	return Error;
}

void URestClient::PublishScoreToLeaderboard(const FString ServerUrl, FScore Score)
{

	TSharedRef<IHttpRequest> HttpRequest = FHttpModule::Get().CreateRequest();

	FString Url = ServerUrl + TEXT("/scores/registered_clients/") + Score.RegisteredClient + TEXT("/");
	HttpRequest->SetURL(Url);
	HttpRequest->SetVerb(TEXT("POST"));
	HttpRequest->SetHeader(TEXT("Content-Type"), TEXT("application/json"));

	FString json = ConvertScoreToJson(Score);

	HttpRequest->SetContentAsString(json);
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &URestClient::OnProcessRequestComplete);

	HttpRequest->ProcessRequest();
}

void URestClient::GetScoresFromLeaderboard(const FString ServerUrl, const FString PlayerUuid, bool BestOfPlayer, const FString OrderBy, const FString Page)
{
	TSharedRef<IHttpRequest> HttpRequest = FHttpModule::Get().CreateRequest();

	FString Url = ServerUrl + TEXT("/scores/");

	if (!BestOfPlayer && !PlayerUuid.IsEmpty())
	{
		Url += TEXT("registered_clients/") + PlayerUuid + TEXT("/");
	}

	FString OrderByValue = OrderBy.IsEmpty() ? this->OrderAltitudeDesc : OrderBy;

	Url += TEXT("?order_by=") + OrderByValue;

	if (!Page.IsEmpty())
	{
		Url += "&page=" + Page;
	}

	if (BestOfPlayer)
	{
		Url += "&bestof=" + PlayerUuid;
	}

	HttpRequest->SetURL(Url);
	HttpRequest->SetVerb(TEXT("GET"));
	HttpRequest->SetHeader(TEXT("Content-Type"), TEXT("application/json"));

	HttpRequest->OnProcessRequestComplete().BindUObject(this, &URestClient::OnProcessRequestComplete);

	HttpRequest->ProcessRequest();
}

FScorePage URestClient::ConvertJsonToScores()
{
	FScorePage ScorePage;

	FString Next = GetStringFromJsonParsed(TEXT("next"), TEXT(""));
	FString Previous = GetStringFromJsonParsed(TEXT("previous"), TEXT(""));

	int32 Count = JsonParsed->GetIntegerField("count");

	ScorePage.Next = Next;
	ScorePage.Previous = Previous;
	ScorePage.Count = Count;
	ScorePage.Page = GetPage(Previous, Next);

	TArray<FScore> Scores;
	TArray <TSharedPtr<FJsonValue>> Results = JsonParsed->GetArrayField("results");

	for (TSharedPtr<FJsonValue> Result : Results)
	{
		FScore Score;
		TSharedPtr<FJsonObject> ResultObject = Result->AsObject();
		Score.UserName = ResultObject->GetStringField("user_name");
		Score.PlayedOn = ResultObject->GetStringField("played_on");
		Score.RecievedOn = ResultObject->GetStringField("recieved_on");
		Score.Playtime = FString::FromInt(ResultObject->GetIntegerField("play_time"));
		Score.Altitude = FString::FromInt(ResultObject->GetIntegerField("altitude"));
		Score.Score = FString::FromInt(ResultObject->GetIntegerField("score"));
		Score.RegisteredClient = ResultObject->GetStringField("registered_client");
		Score.Country = ResultObject->GetStringField("country");
		Score.State = ResultObject->GetStringField("state");
		Score.City = ResultObject->GetStringField("city");

		Scores.Add(Score);
	}

	ScorePage.Scores = Scores;

	return ScorePage;
}

FString URestClient::GetStringFromJsonParsed(const FString Key, const FString Default)
{
	FString Value = Default;

	TSharedPtr<FJsonValue> ValueT = JsonParsed->TryGetField(Key);
	if (ValueT.IsValid())
	{
		Value = JsonParsed->GetStringField(Key);
	}

	return Value;
}

int32 URestClient::GetPage(FString Previous, FString Next)
{
	FString Pattern = TEXT("page=([0-9]+)");
	FString PreviousPage = UBubbleUtils::FindFirstMatch(Pattern, Previous);

	if (Previous.IsEmpty())
	{
		// "previous": null 
		return 1;
	}
	else if (PreviousPage.IsEmpty())
	{
		// "previous": "http://localhost:8000/scores/"
		return 2;
	}

	return FCString::Atoi(*PreviousPage) + 1;
}

// http://www.wraiyth.com/?p=198
FString URestClient::ConvertScoreToJson(FScore Score)
{
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
	JsonObject->SetStringField("user_name", Score.UserName);
	JsonObject->SetStringField("played_on", Score.GetPlayedOnOrNow());
	JsonObject->SetNumberField("play_time", Score.GetPlaytime());
	JsonObject->SetNumberField("altitude", Score.GetAltitude());
	JsonObject->SetNumberField("score", Score.GetScore());
	JsonObject->SetStringField("registered_client", Score.RegisteredClient);

	FString OutputString;
	TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&OutputString);
	FJsonSerializer::Serialize(JsonObject.ToSharedRef(), Writer);

	return OutputString;
}

void URestClient::OnProcessRequestComplete(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{

	// Check we have result to process futher
	if (!bWasSuccessful)
	{
		//UE_LOG(RestLog, Error, TEXT("Request failed: %s"), *Request->GetURL());
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::White, TEXT("Request failed"));

		// Broadcast the result event
		OnRequestFail.Broadcast(this);

		return;
	}

	// Save response data as a string
	ResponseContent = Response->GetContentAsString();

	// Save response code as int32
	ResponseCode = Response->GetResponseCode();

	// Log response state
	//UE_LOG(RestLog, Log, TEXT("Response (%d): %s"), Response->GetResponseCode(), *Response->GetContentAsString());
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::White, *Response->GetContentAsString());

	// Process response headers
	TArray<FString> Headers = Response->GetAllHeaders();
	for (FString Header : Headers)
	{
		FString Key;
		FString Value;
		if (Header.Split(TEXT(": "), &Key, &Value))
		{
			ResponseHeaders.Add(Key, Value);
		}
	}

	// Try to deserialize data to JSON
	TSharedRef<TJsonReader<TCHAR>> JsonReader = TJsonReaderFactory<TCHAR>::Create(ResponseContent);
	FJsonSerializer::Deserialize(JsonReader, JsonParsed);

	// Decide whether the request was successful
	bIsValidJsonResponse = bWasSuccessful && JsonParsed.IsValid();

	// Log errors
	if (!bIsValidJsonResponse)
	{
		if (!JsonParsed.IsValid())
		{
			// As we assume it's recommended way to use current class, but not the only one,
			// it will be the warning instead of error
			UE_LOG(RestLog, Warning, TEXT("JSON could not be decoded!"));
		}
	}

	// Broadcast the result event
	OnRequestComplete.Broadcast(this);
}
