// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "GenericPlatformMath.h"
#include "Core.h"
#include "CoreUObject.h"
#include "Regex.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BubbleUtils.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT6_API UBubbleUtils : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "BubbleUtils|DateManipulation")
	static FString NormalizeDate(const FString Date);

	UFUNCTION(BlueprintCallable, Category = "BubbleUtils|DateManipulation")
	static FString DecisecondsToClock(const FString Deciseconds);

	UFUNCTION(BlueprintCallable, Category = "BubbleUtils|DateManipulation")
	static int32 ClockToDecisecond(const FString Clock);

	UFUNCTION(BlueprintCallable, Category = "BubbleUtils|DateManipulation")
	static FString ZeroPaddedDecimal(const int32 Decimal);

	UFUNCTION(BlueprintCallable, Category = "BubbleUtils|Random")
	static FString GenerateRandomPhrase();

	UFUNCTION(BlueprintCallable, Category = "BubbleUtils|Regexp")
	static FString FindFirstMatch(FString Pattern, FString Text);

	UFUNCTION(BlueprintCallable, Category = "BubbleUtils|Measure")
	static int32 NormaliseAltitude(const float RealAltitude);

	UFUNCTION(BlueprintCallable, Category = "BubbleUtils|Measure")
	static int32 SmartAtoi(FString Ascii);
	
};
