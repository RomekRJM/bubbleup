// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject6.h"
#include "FileHelper.h"

DEFINE_LOG_CATEGORY(FileHelperLog);

bool UFileHelper::WriteStringToFile(
	FString JoyfulFileName,
	FString SaveText,
	bool AllowOverWriting
	){

	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();

	// Get absolute file path
	FString Path = UFileHelper::GetGameFilePath(JoyfulFileName);
	UE_LOG(FileHelperLog, Log, TEXT("WriteStringToFile: %s path: %s"), *JoyfulFileName, *Path);

	// Allow overwriting or file doesn't already exist
	if (AllowOverWriting || !PlatformFile.FileExists(*Path))
	{
		return FFileHelper::SaveStringToFile(SaveText, *Path);
	}

	return false;
}

FString UFileHelper::ReadStringFromFile(
	FString JoyfulFileName
	){
	//get complete file path
	FString Path = UFileHelper::GetGameFilePath(JoyfulFileName);
	UE_LOG(FileHelperLog, Log, TEXT("ReadStringFromFile: client.cfg path: %s"), *Path);

	FString Result;
	FFileHelper::LoadFileToString(Result, *Path);

	return Result;
}

FString UFileHelper::GetGameFilePath(FString JoyfulFileName)
{
	return FPaths::GameDir() + JoyfulFileName;
}

FConfigs UFileHelper::ReadConfigs()
{
	FString ConfigFileContent = UFileHelper::ReadStringFromFile(CONFIG_FILE);
	TMap<FString, FString> Map;
	TArray<FString> KeyValuePairs;
	ConfigFileContent.ParseIntoArrayLines(KeyValuePairs, false);

	for (int32 i = 0; i < KeyValuePairs.Num(); ++i) {
		TArray<FString> KeyValue;
		KeyValuePairs[i].ParseIntoArray(KeyValue, TEXT("="), false);

		if (KeyValue.Num() > 0)
		{
			Map.Add(KeyValue[0], KeyValue[1]);
		}
	}

	FConfigs Configs;

	if (Map.Contains(UUID))
	{
		Configs.Uuid = Map[UUID];
	}

	if (Map.Contains(SERVER_URL))
	{
		Configs.ServerUrl = Map[SERVER_URL];
	}

	if (Map.Contains(REGISTERED_CLIENT))
	{
		Configs.RegisteredClient = Map[REGISTERED_CLIENT];
	}

	if (Map.Contains(PHRASE))
	{
		Configs.Phrase = Map[PHRASE];
	}

	if (Map.Contains(NUM_COMPLETED))
	{
		Configs.NumCompleted = Map[NUM_COMPLETED];
	}

	return Configs;
}

bool UFileHelper::WriteConfigs(FConfigs Configs)
{
	FString ConfigFileContent = TEXT("");
	ConfigFileContent.Append(UUID).Append("=").Append(Configs.Uuid).Append("\r\n");
	ConfigFileContent.Append(SERVER_URL).Append("=").Append(Configs.ServerUrl).Append("\r\n");
	ConfigFileContent.Append(REGISTERED_CLIENT).Append("=").Append(Configs.RegisteredClient).Append("\r\n");
	ConfigFileContent.Append(PHRASE).Append("=").Append(Configs.Phrase).Append("\r\n");
	ConfigFileContent.Append(NUM_COMPLETED).Append("=").Append(Configs.NumCompleted).Append("\r\n");

	return UFileHelper::WriteStringToFile(CONFIG_FILE, ConfigFileContent, true);
}