// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject6.h"
#include "BubbleUtils.h"


FString UBubbleUtils::NormalizeDate(const FString Date)
{
	FString NormDate = Date.Replace(TEXT("T"), TEXT(" "));
	NormDate = NormDate.Replace(TEXT("Z"), TEXT(""));

	int32 DotPosition;

	if (NormDate.FindLastChar('.', DotPosition))
	{
		NormDate = NormDate.Left(DotPosition);
	}

	return NormDate;
}

FString UBubbleUtils::GenerateRandomPhrase()
{
	FString AllowedWords[3][13] = {
		{ TEXT("giant"), TEXT("squealing"), TEXT("empowered"), TEXT("frowning"), TEXT("starving"),
		TEXT("violet"), TEXT("astonishing"), TEXT("lurking"), TEXT("nice"), TEXT("slender"), TEXT("fluffy"),
		TEXT("confident"), TEXT("comely") },
		{ TEXT("drunk"), TEXT("sober"), TEXT("raging"), TEXT("fearless"), TEXT("dignified"),
		TEXT("straight"), TEXT("wise"), TEXT("coarse"), TEXT("confused"), TEXT("abstract"), TEXT("wandering"), TEXT("shiny"), TEXT("dim") },
		{ TEXT("dog"), TEXT("ox"), TEXT("fox"), TEXT("penguin"), TEXT("cat"), TEXT("lynx"),
		TEXT("aurochs"), TEXT("panda"), TEXT("sparrow"), TEXT("squirell"), TEXT("alpaka"), TEXT("ram"), TEXT("panda")
		}
	};

	FString Phrase = TEXT("");
	int32 AllowedWordsLength = ARRAY_COUNT(AllowedWords);
	for (int32 i = 0; i < AllowedWordsLength; ++i)
	{
		int32 index = FMath::RandRange(0, ARRAY_COUNT(AllowedWords[i]) - 1);
		Phrase += AllowedWords[i][index];

		if (i < AllowedWordsLength - 1)
		{
			Phrase += TEXT(" ");
		}
	}

	return Phrase;
}

FString UBubbleUtils::FindFirstMatch(FString Pattern, FString Text)
{
	FString FirstMatch = TEXT("");
	FRegexPattern PagePattern(Pattern);
	FRegexMatcher PageMatcher(PagePattern, Text);

	if (PageMatcher.FindNext())
	{
		FirstMatch = PageMatcher.GetCaptureGroup(1);
	}

	return FirstMatch;
}

FString UBubbleUtils::DecisecondsToClock(const FString Decisecond)
{
	int32 DecisecondInt = FCString::Atoi(*Decisecond);
	int32 Deciseconds = DecisecondInt % 100;
	int32 Seconds = ((DecisecondInt - Deciseconds) / 100) % 60;
	int32 Minutes = (DecisecondInt - Seconds*100) / 6000;

	return ZeroPaddedDecimal(Minutes) + TEXT(":") + ZeroPaddedDecimal(Seconds) + TEXT(":") + ZeroPaddedDecimal(Deciseconds);
}

int32 UBubbleUtils::ClockToDecisecond(const FString Clock)
{
	FString Decisecond;
	FString Minute;
	FString Rest;
	FString Second;

	if (Clock.Split(TEXT(":"), &Minute, &Rest))
	{
		if (Rest.Split(TEXT(":"), &Second, &Decisecond))
		{
			return 100 * (FCString::Atoi(*Minute) * 60 + FCString::Atoi(*Second)) + FCString::Atoi(*Decisecond);
		}
	}

	return -1;
}

FString UBubbleUtils::ZeroPaddedDecimal(const int32 Decimal)
{
	return (Decimal >= 10 ? TEXT("") : TEXT("0")) + FString::FromInt(Decimal);
}

int32 UBubbleUtils::NormaliseAltitude(const float RealAltitude)
{
	return FGenericPlatformMath::Round(RealAltitude / 16.97) - 31;
}

int32 UBubbleUtils::SmartAtoi(FString Ascii)
{
	return FCString::Atoi(*Ascii.
		Replace(TEXT(","), TEXT("")).
		Replace(TEXT("."), TEXT("")).
		Replace(TEXT(" "), TEXT("")));
}