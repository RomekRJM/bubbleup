// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Http.h"
#include "Json.h"

#include "Engine.h"
#include "Core.h"
#include "CoreUObject.h"
#include "ModuleManager.h"
#include "Delegate.h"
#include "SharedPointer.h"
#include "BubbleUtils.h"

DECLARE_LOG_CATEGORY_EXTERN(RestLog, Log, Log);

#include "Kismet/BlueprintFunctionLibrary.h"
#include "RestClient.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRequestComplete, class URestClient*, Request);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRequestFail, class URestClient*, Request);

USTRUCT()
struct FScore {
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RestPlugin|Score")
	FString UserName;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RestPlugin|Score")
	FString PlayedOn;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RestPlugin|Score")
	FString RecievedOn;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RestPlugin|Score")
	FString Playtime;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RestPlugin|Score")
	FString Altitude;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RestPlugin|Score")
	FString Score;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RestPlugin|Score")
	FString RegisteredClient;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RestPlugin|Score")
	FString Country;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RestPlugin|Score")
	FString State;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RestPlugin|Score")
	FString City;

	int32 GetPlaytime()
	{
		return UBubbleUtils::ClockToDecisecond(Playtime);
	}

	int32 GetScore()
	{
		return UBubbleUtils::SmartAtoi(*Score);
	}

	int32 GetAltitude()
	{
		return UBubbleUtils::SmartAtoi(*Altitude);
	}

	FString GetPlayedOnOrNow()
	{
		if (PlayedOn.IsEmpty())
		{
			FDateTime Now = FDateTime::UtcNow();
			return Now.ToIso8601();
		}

		return PlayedOn;
	}

	FScore()
	{
		
	}

};

USTRUCT()
struct FScorePage
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RestPlugin|ScorePage")
	TArray<FScore> Scores;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RestPlugin|ScorePage")
	int32 Count;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RestPlugin|ScorePage")
	int32 Page;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RestPlugin|ScorePage")
	FString Next;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RestPlugin|ScorePage")
	FString Previous;
};

/**
 * 
 */
UCLASS(BlueprintType, BLueprintable)
class URestClient : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()

	URestClient();

public:

	UFUNCTION(BlueprintCallable, Category = "RestPlugin")
	static URestClient* GetRestClient();

	UFUNCTION(BlueprintCallable, Category = "RestPlugin")
	void GetUUIDFromServer(const FString ServerUrl, const FString Phrase, const FString Name);

	UFUNCTION(BlueprintCallable, Category = "RestPlugin")
	FString CreateRegisteredClientJson(const FString Phrase, const FString Name);

	UFUNCTION(BlueprintCallable, Category = "RestPlugin")
	void PublishScoreToLeaderboard(const FString ServerUrl, FScore Score);

	UFUNCTION(BlueprintCallable, Category = "RestPlugin")
	void GetScoresFromLeaderboard(const FString ServerUrl, const FString PlayerUuid, bool BestOfPlayer, const FString OrderBy, const FString Page);

	UFUNCTION(BlueprintCallable, Category = "RestPlugin")
	FScorePage ConvertJsonToScores();

	UFUNCTION(BlueprintCallable, Category = "RestPlugin")
	FString ConvertScoreToJson(FScore Score);

	UFUNCTION(BlueprintCallable, Category = "RestPlugin")
	FString GetStringFromJsonParsed(const FString Key, const FString Default);

	UFUNCTION(BlueprintCallable, Category = "RestPlugin")
	int32 GetPage(FString Previous, FString Next);

	UFUNCTION(BlueprintCallable, Category = "RestPlugin")
	FString GetUUIDFromServerResponse();

	UFUNCTION(BlueprintCallable, Category = "RestPlugin")
	FString GetUserNameFromServerResponse();

	UFUNCTION(BlueprintCallable, Category = "RestPlugin")
	FString GetErrorFromServerResponse();

	UPROPERTY(BlueprintAssignable, Category = "RestPlugin|Event")
	FOnRequestComplete OnRequestComplete;

	UPROPERTY(BlueprintAssignable, Category = "RestPlugin|Event")
	FOnRequestFail OnRequestFail;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RestPLugin|Response")
	FString ResponseContent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RestPlugin|Response")
	bool bIsValidJsonResponse;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RestPlugin|Constants")
	FString FirstPage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RestPlugin|Constants")
	FString LastPage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RestPlugin|Constants")
	FString OrderScoreDesc;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RestPlugin|Constants")
	FString OrderAltitudeDesc;


protected:
	int32 ResponseCode;

	TMap<FString, FString> ResponseHeaders;

	TSharedPtr<FJsonObject> JsonParsed;

	FWeakObjectPtr Request;


private:
	void OnProcessRequestComplete(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	
};
